package com.example.java.controller;

import com.example.java.service.BookService;
import lombok.RequiredArgsConstructor;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class JavaController{
private final BookService bookService;

 @GetMapping
  public Result<Record> getBookAuthor(){
     return bookService.getBookAuthor();
 }
}
