package com.example.java.controller;


import com.example.java.repository.AuthorRepository;
import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Author;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("/api/authors")
public class AuthorController {
    private final AuthorRepository authorRepository;

    @GetMapping
    public List<Author> getAll(){
        return authorRepository.findAll();
    }
}
