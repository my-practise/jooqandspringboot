package com.example.java.service;


import com.example.java.exception.BadRequestException;
import com.example.java.repository.BookRepository;
import com.example.java.exception.DataNotFoundException;
import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Book;
import lombok.RequiredArgsConstructor;

import org.jooq.Record;
import org.jooq.Result;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class BookService {
  private final BookRepository bookRepository;

  public Book create(Book book){
    return bookRepository.save(book);

  }

  public List<Book> getAll(){
      return bookRepository.findAll();
  }

  public Book getOne(int id){
      Optional<Book> book = bookRepository.findById(id);
      if(book.isEmpty()){
          throw new DataNotFoundException(MessageFormat.format("Book id {0} not found", String.valueOf(id)));
      }
      return book.get();
  }

  public void deleteById(int id){
      boolean isDeleted = bookRepository.deleteById(id);
      if(!isDeleted){
          throw new BadRequestException("Delete error, please check ID and try again");
      }
  }
  public Result<Record> getBookAuthor(){
      return bookRepository.fetchDataFromTables();
  }


}
