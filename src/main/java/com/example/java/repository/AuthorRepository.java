package com.example.java.repository;

import com.tej.JooQDemo.jooq.sample.model.Tables;
import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Author;
import com.tej.JooQDemo.jooq.sample.model.tables.records.AuthorRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Transactional
@Repository
public class AuthorRepository implements JOOQRepository<Author> {

    private final DSLContext dslContext;
    @Override
    public Author save(Author author) {
        AuthorRecord authorRecord = dslContext.insertInto(Tables.AUTHOR)
                .set(Tables.AUTHOR.FIRSTNAME,author.getFirstname())
                .set(Tables.AUTHOR.LASTNAME,author.getLastname())
                .returning(Tables.AUTHOR.ID).fetchOne();
        if(authorRecord != null){
            author.setId(authorRecord.getId());
            return author;
        }
        return null;
    }

    @Override
    public Author update(Author author, int id) {
        AuthorRecord authorRecord = dslContext.update(Tables.AUTHOR)
                .set(Tables.AUTHOR.FIRSTNAME,author.getFirstname())
                .set(Tables.AUTHOR.LASTNAME,author.getLastname())
                .where(Tables.AUTHOR.ID.eq(id))
                .returning(Tables.AUTHOR.ID).fetchOne();

        return (authorRecord!=null)? author : null;
    }

    @Override
    public List<Author> findAll() {

        return dslContext.selectFrom(Tables.AUTHOR).fetchInto(Author.class);
    }

    @Override
    public Optional<Author> findById(int id) {
        Author author = dslContext.selectFrom(Tables.AUTHOR).where(Tables.AUTHOR.ID.eq(id)).fetchOneInto(Author.class);

        return (ObjectUtils.isEmpty(author))? Optional.empty():Optional.of(author);
    }

    @Override
    public boolean deleteById(int id) {
        return dslContext.delete(Tables.AUTHOR).where(Tables.AUTHOR.ID.eq(id)).execute()==1;
    }
}
