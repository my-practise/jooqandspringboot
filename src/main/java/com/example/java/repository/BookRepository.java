package com.example.java.repository;


import com.example.java.repository.JOOQRepository;
import com.tej.JooQDemo.jooq.sample.model.Tables;
import com.tej.JooQDemo.jooq.sample.model.tables.records.BookRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Book;


import java.util.List;
import java.util.Optional;



@RequiredArgsConstructor
@Transactional
@Repository
public class BookRepository implements JOOQRepository<Book> {


    private final DSLContext context;
    @Override
    public Book save(Book book) {
        BookRecord bookRecord = context.insertInto(Tables.BOOK)
                .set(Tables.BOOK.DESCRIPTION, book.getDescription())
                .set(Tables.BOOK.ISBN, book.getIsbn())
                .set(Tables.BOOK.PAGE, book.getPage())
                .set(Tables.BOOK.PRICE, book.getPrice())
                .set(Tables.BOOK.TITLE, book.getTitle())
                .set(Tables.BOOK.AUTHOR_ID, book.getAuthorId())
                .returning(Tables.BOOK.ID).fetchOne();

        if (bookRecord != null) {
            book.setId(bookRecord.getId());
            return book;
        }
        return null;

    }

    @Override
    public Book update(Book book,int id) {
       BookRecord bookRecord = context.update(Tables.BOOK)
               .set(Tables.BOOK.DESCRIPTION,book.getDescription())
               .set(Tables.BOOK.ISBN,book.getIsbn())
               .set(Tables.BOOK.PAGE,book.getPage())
               .set(Tables.BOOK.PRICE,book.getPrice())
               .set(Tables.BOOK.TITLE,book.getTitle())
               .set(Tables.BOOK.AUTHOR_ID,book.getAuthorId())
               .where(Tables.BOOK.ID.eq(id))
               .returning(Tables.BOOK.ID).fetchOne();

        return (bookRecord != null) ? book : null;

    }

    @Override
    public List<Book> findAll() {
        return context.selectFrom(Tables.BOOK)
                .fetchInto(Book.class);
    }

    @Override
    public Optional<Book> findById(int id) {
        Book book = context.selectFrom(Tables.BOOK).where(Tables.BOOK.ID.eq(id)).fetchOneInto(Book.class);
        return (ObjectUtils.isEmpty(book)) ? Optional.empty() : Optional.of(book);
    }

    @Override
    public boolean deleteById(int id) {
        return context.delete(Tables.BOOK)
                .where(Tables.BOOK.ID.eq(id))
                .execute()==1;
    }

    public Result<Record> fetchDataFromTables(){
        return  context.select().from(Tables.BOOK)
                .join(Tables.AUTHOR)
                .on(Tables.AUTHOR.ID.eq(Tables.BOOK.AUTHOR_ID)).fetch();
    }
}
